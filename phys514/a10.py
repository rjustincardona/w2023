from scipy.constants import *
import numpy as np

# Q2
print("\nQUESTION 2")
time = 24 * 60 * 60
M = 5.972e24
rr = 6.378e6
rs = rr + 2e7
R = 2 * G * M / c**2
gamma = np.sqrt((1 - (R / rs)) / (1 - R / rr))
print('{:.2e}'.format(gamma - 1),\
        '{:.2e}'.format((gamma - 1) * time),\
        '{:.2e}'.format((gamma - 1) * time * c))

# Q3
print("\nQUESTION 3")
from decimal import *
M_sun = Decimal (1.989e30)
c = Decimal(c)
G = Decimal(G)
m1 = Decimal (38.9) * M_sun
a1 = Decimal(0.32) * G * m1 / c**2

m2 = Decimal(31.6) * M_sun
a2 = Decimal(0.44) * G * m2 / c**2

Mf = Decimal(67.4) * M_sun
af = Decimal(0.67) * G * Mf / c**2

def rplus(m, a):
    return (G * m / c**2) + Decimal(np.sqrt((G * m / c**2)**2 - a**2))
def A(m, a):
    return Decimal(4) * Decimal(pi) * (a**2 + rplus(m, a)**2)
print('{:.2e}'.format(A(m1, a1)),\
        '{:.2e}'.format(A(m2, a2)),\
        '{:.2e}'.format(A(Mf, af)))

# Q5
print("\nQUESTION 5")
c = float(c)
G = float(G)
M_sun =1.989e30
b_sun = 6.957e8
au = 1.496e11
b_venus = 6.0518e6
m_venus = 4.867e24
r_venus = 0.7 * au

def t(r, b, m):
    R = 2 * G * m / c**2
    return R * np.log(np.abs((np.sqrt(r**2-b**2) + r) / b**2))

dt = (t(r_venus, b_sun, M_sun) + t(au, b_sun, M_sun)) / c
mins = dt / 60
days = mins / (60 * 24)
print('{:.2e}'.format(dt))


# Q11
print("\nQUESTION 11")
n = 2
def a(M):
    N = M / m_p
    num = (3 ** (7/3)) * (hbar**2) * (pi ** (2/3)) * (N ** (5/3))
    den = (2 ** (1/3)) * 20 * m_e * (n ** (5/3))
    return num / den

def L(M): return 10 * a(M) / (3 * G * (M ** 2))

def p(M):
    N = M / m_p
    E = a(M) / (L(M) ** 2)
    return np.sqrt(2 * m_e * E / N) / (m_e * c)

M_c = 8 * np.sqrt(10 * pi) * ((hbar * c) ** (3/2)) / (9 * (G ** (3/2)) * (m_p**2) * (n**2))
M_c /= M_sun
M_c_new = np.sqrt(28 / 43) * M_c

print('{:.2e}'.format(L(M_sun)), '{:.2e}'.format(p(M_sun)), '{:.2e}'.format(M_c), '{:.2e}'.format(M_c_new))
