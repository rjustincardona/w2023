import numpy as np
from sympy.parsing.sympy_parser import parse_expr
from sympy import *
import pickle

# QUESTION 1
def christoffel_symbol(g, s, verbose=False, simp=False):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
            verbose: [bool] Whether or not to print the progress of the computation
            simp: [bool] Whether or not to simplify the result
        OUTPUT
            result: [Sympy Array] The Christoffel Symbols corresponding to the metric and its coordinates
    """
    D = g.shape[1]
    x = symbols(s)
    ginv = g.inv()
    result = [[[parse_expr("0") for i in range(D)] for j in range(D)] for k in range(D)]
    result = np.array(result)
    for mu in range(D):
        for a in range(D):
            for b in range(D):
                temp = parse_expr("0")
                for l in range(D):
                    temp = temp + ginv[mu, l] * (diff(g[l, a], x[b]) + diff(g[l, b], x[a]) - diff(g[a, b], x[l]))
                if simp:
                    result[mu, a, b] = simplify(temp / 2)
                else:
                    result[mu, a, b] = temp / 2
                if verbose: print("G[" + str(mu) + " " + str(a) + " " + str(b) + "] = " + str(result[mu, a, b]))
    result = Array([[[result[i, j, k] for k in range(D)] for j in range(D)] for i in range(D)])
    if verbose: print("christoffel_symbol_done")
    return result


def riemann_tensor(g, s, verbose=False, simp=False):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
            verbose: [bool] Whether or not to print the progress of the computation
            simp: [bool] Whether or not to simplify the result
        OUTPUT
            result: [Sympy Array] The Riemann Tensor corresponding to the metric and its coordinates
    """
    D = g.shape[1]
    x = symbols(s)
    G = christoffel_symbol(g, s, verbose, simp)
    result = [[[[parse_expr("0") for i in range(D)] for j in range(D)] for k in range(D)] for l in range(D)]
    result = np.array(result)
    for r in range(D):
        for s in range(D):
            for mu in range(D):
                for nu in range(D):
                    temp = diff(G[r, nu, s], x[mu]) - diff(G[r, mu, s], x[nu])
                    for l in range(D):
                        temp = temp + G[r, mu, l] * G[l, nu, s] - G[r, nu, l] * G[l, mu, s]
                    if simp:
                        result[r, s, mu, nu] = simplify(temp)
                    else:
                        result[r, s, mu, nu] = temp
                    if verbose: print("R[" + str(r) + " " + str(s) + " " + str(mu) + " " + str(nu) + "] = " + str(result[r, s, mu, nu]))
    result = Array([[[[result[i, j, k, l] for l in range(D)] for k in range(D)] for j in range(D)] for i in range(D)])
    if verbose: print("riemann_tensor_done")
    return result


def ricci_tensor(g, s, verbose=False, simp=False):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
            verbose: [bool] Whether or not to print the progress of the computation
            simp: [bool] Whether or not to simplify the result
        OUTPUT
            result: [Sympy Matrix] The Ricci Tensor corresponding to the metric and its coordinates
    """
    D = g.shape[1]
    R = riemann_tensor(g, s, verbose, simp)
    result = [[parse_expr("0") for i in range(D)] for j in range(D)]
    result = np.array(result)
    for mu in range(D):
        for nu in range(D):
            temp = parse_expr("0")
            for r in range(D):
                temp = temp + R[r, mu, r, nu]
            if simp:
                result[mu, nu] = simplify(temp)
            else:
                result[mu, nu] = temp
            if verbose: print("R[" + str(mu) + " " + str(nu) + "] = " + str(result[mu, nu]))
    result = Array([[result[i, j] for j in range(D)] for i in range(D)]).tomatrix()
    if verbose: print("ricci_tensor_done")
    return result


def ricci_scalar(g, s, verbose=False, simp=False):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
            verbose: [bool] Whether or not to print the progress of the computation
            simp: [bool] Whether or not to simplify the result
        OUTPUT
            result: [Sympy Expression] The Ricci Scalar corresponding to the metric and its coordinates
    """
    D = g.shape[1]
    ginv = g.inv()
    R = ricci_tensor(g, s, verbose, simp)
    result = parse_expr("0")
    for mu in range(D):
        for nu in range(D):
            result = result + ginv[mu, nu] * R[mu, nu]
            if verbose: print(str(mu) + " " + str(nu))
    if verbose: print("ricci_scalar_done")
    if simp:
        return simplify(result)
    return result
    

def einstein_tensor(g, s, verbose=False, simp=False):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
            verbose: [bool] Whether or not to print the progress of the computation
            simp: [bool] Whether or not to simplify the result
        OUTPUT
            result: [Sympy Matrix] The Einstein Tensor corresponding to the metric and its coordinates
    """
    result = ricci_tensor(g, s, verbose, simp) - (ricci_scalar(g, s, verbose, simp) / 2) * g
    if verbose: print("einstein_tensor_done")
    return result

def geodesic_equations(g, s):
    """
        INPUT
            g: [Sympy Matrix] The metric's components
            s: [String] Space separated coordinates symbols, ordered by index
        OUTPUT
            result: [List] List of string depicting the lightlike geodesic equations for the given metric and its coordinates
    """
    D = g.shape[1]
    G = christoffel_symbol(g, s, simp=True)
    result = []
    for a in range(D):
        temp = "x" + str(a) + "\'\'"
        for mu in range(D):
            for nu in range(D):
                if str(G[a, mu, nu]) != "0":
                    temp += " + " + str(G[a, mu, nu]) + " x" + str(mu) + "\'x" + str(nu) + "\'"
        temp += " = 0"
        result.append(temp)
    return result


# QUESTION 2 & 4
tp = "-2 * b * r**2"
pp = "(1 - (b**2) * (r**2)) * (r**2)"
rr = "exp(-((b**2) * (r**2)))"

g_sc = [["-1", tp, "0", "0"],
        [tp, pp, "0", "0"],
        ["0", "0", rr, "0"], 
        ["0", "0", "0", rr]] 
g_sc = Array([[parse_expr(i) for i in j] for j in g_sc]).tomatrix()
s_sc = "t p r z"
print("Einstein Tensor:")
for i in einstein_tensor(g_sc, s_sc, False, True):
    print_latex(simplify(simplify(i)))
