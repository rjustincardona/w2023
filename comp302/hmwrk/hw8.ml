(**To DO: Write a good set of tests for free_variables **)
let free_variables_tests = [
  (* An example test case.
     Note that you are *only* required to write tests for Let, Rec, Fn, and Apply!
  *)
  (Let ("x", I 1, I 5), []);
  (Let ("x", I 1, Var "x"), []);
  (Rec ("x", Int, If (Var "x", I 5, B true )), []);
  (Fn ([("x", Int); ("y", Int)], If (Var "x", I 5, B true )), []);
  (Apply (Var "x", [Var "x";  If (Var "x", I 5, B true )]), ["x"]);
]

(* TODO: Implement the missing cases of free_variables. *)
let rec free_variables : exp -> name list =
  (* Taking unions of lists.
     If the lists are in fact sets (all elements are unique),
     then the result will also be a set.
  *)
  let union l1 l2 = delete l2 l1 @ l2 in
  let union_fvs es =
    List.fold_left (fun acc exp -> union acc (free_variables exp)) [] es
  in
  function
  | Var y -> [y]
  | I _ | B _ -> []
  | If(e, e1, e2) -> union_fvs [e; e1; e2]
  | Primop (_, args) -> union_fvs args
  | Fn (xs, e) ->
      delete (List.map (fun x -> let (a, _) = x in a) xs) (union_fvs [e])
  | Rec (x, _, e) ->
      delete [x] (union_fvs [e])
  | Let (x, e1, e2) ->
      delete [x] (union_fvs [e1; e2])
  | Apply (e, es) -> union (union_fvs [e]) (union_fvs es)


(* TODO: Write a good set of tests for subst. *)
(* Note: we've added a type annotation here so that the compiler can help
   you write tests of the correct form. *)
let subst_tests : (((exp * name) * exp) * exp) list = [
  (* An example test case. If you have trouble writing test cases of the
     proper form, you can try copying this one and modifying it.
     Note that you are *only* required to write tests for Rec, Fn, and Apply!
  *)
  (((I 1, "x"), (* [1/x] *)
    (* let y = 2 in y + x *)
    Let ("y", I 2, Primop (Plus, [Var "y"; Var "x"]))),
   (* let y = 2 in y + 1 *)
   Let ("y", I 2, Primop (Plus, [Var "y"; I 1])));
  
  (((I 1, "x"), 
    Rec ("y", Int, Primop (Plus, [Var "y"; Var "x"]))),
   Rec ("y", Int, Primop (Plus, [Var "y"; I 1])));
  
  (((I 1, "y"), 
    Fn([("x", Int)], Primop (Plus, [Var "y"; Var "x"]))),
   Fn([("x", Int)], Primop (Plus, [I 1; Var "x"])));
  
  (((I 1, "y"),
    Apply (Var "x", [Var "y"])),
   Apply (Var "x", [I 1]));
  
  (((I 1, "y"), 
    Fn ([("x", Int); ("y", Int)],
        Primop (Plus,
                [Primop (Times, [Var "x"; Var "x"]);
                 Primop (Times, [Var "y"; Var "y"])]))),
   Fn ([("x", Int); ("y", Int)],
       Primop (Plus,
               [Primop (Times, [Var "x"; Var "x"]);
                Primop (Times, [Var "y"; Var "y"])])));
  
  (((Var "x", "y"), 
    Rec ("y", Int, Primop (Plus, [Var "y"; Var "x"]))),
   Rec ("y", Int, Primop (Plus, [Var "y"; Var "x"])));
  
  (((Var "x", "y"), 
    Fn([("x", Int)], Primop (Plus, [Var "y"; Var "x"]))),
   Fn([("x1", Int)], Primop (Plus, [Var "x"; Var "x1"])));
  
  (((Var "x", "y"), 
    Rec("x", Int, Primop (Plus, [Var "y"; Var "x"]))),
   Rec("x1", Int, Primop (Plus, [Var "x"; Var "x1"])));
  
  (((Let ("x", I 5, Var "y"), "y"),
    Apply (Var "x", [Let ("x", I 5, Var "y")])),
   Apply (Var "x", [Let ("x", I 5, Let ("x", I 5, Var "y"))]));
  
  (((I 3, "y"),
    Apply (Fn ([("x", Int)],
               Primop (Plus,
                       [Primop (Times, [Var "x"; Var "x"]);
                        Primop (Times, [Var "y"; Var "y"])])),
           [Var "x"; Var "y"])),
   Apply (Fn ([("x", Int)],
              Primop (Plus,
                      [Primop (Times, [Var "x"; Var "x"]);
                       Primop (Times, [I 3; I 3])])),
          [Var "x"; I 3]))
]

(* TODO: Implement the missing cases of subst. *)
let rec subst ((e', x) as s) exp =
  match exp with
  | Var y ->
      if x = y then e'
      else Var y
  | I n -> I n
  | B b -> B b
  | Primop (po, args) -> Primop (po, List.map (subst s) args)
  | If (e, e1, e2) ->
      If (subst s e, subst s e1, subst s e2)
  | Let (y, e1, e2) ->
      let e1' = subst s e1 in
      if y = x then
        Let (y, e1', e2)
      else
        let (y, e2) =
          if List.mem y (free_variables e') then
            rename y e2
          else
            (y, e2)
        in
        Let (y, e1', subst s e2)

  | Rec (y, t, e) -> 
      let (y, e) = if List.mem y (free_variables e') then
          rename y e else
          (y, e) in
      Rec (y, t, if x = y then e else subst s e)

  | Fn (xs, e) -> 
      let rec helper xs e vars = match xs with
        | [] -> (vars, e)
        | (y, yt)::is -> 
            if List.mem y (free_variables e') then 
              let (y, e) = rename y e in
              if (List.mem y (free_variables e)) then
                helper is (if (x = y)  then e else subst s e) (vars @ [(y, yt)])
              else
                helper is (subst s e) (vars @ [(y, yt)]) 
            else
              let (y, e) = (y, e) in
              if (List.mem y (free_variables e)) then
                helper is (if x = y then e else subst s e) (vars @ [(y, yt)])
              else
                helper is e (vars @ [(y, yt)]) in
      let (a, b) = helper xs e [] in
      Fn (a, b)
        
        
  | Apply (e, es) -> Apply(subst s e, List.map (fun a -> subst s a) es)

and rename x e =
  let x' = freshVar x in
  (x', subst (Var x', x) e)

and rename_all names exp =
  List.fold_right
    (fun name (names, exp) ->
       let (name', exp') = rename name exp in
       (name' :: names, exp'))
    names
    ([], exp)

(* Applying a list of substitutions to an expression, leftmost first *)
let subst_list subs exp =
  List.fold_left (fun exp sub -> subst sub exp) exp subs
