(* Question 1: Manhattan Distance *)
(* TODO: Write a good set of tests for distance. *)
let distance_tests = [
    (* Your test cases go here *)
  (((1, 2), (1, 2)), 0);
  (((1, 2), (4, 7)), 8);
  (((-3, 0), (-4, 5)), 6);
]
;;

(* TODO: Correct this implementation so that it compiles and returns
         the correct answers.
*) 
(* Compute absolute difference*)
let abs_diff x y = if x > y then x - y else y - x

let distance (x1, y1) (x2, y2) =
  (abs_diff x1 x2) + (abs_diff y1 y2)
                     
                     


(* Question 2: Binomial *)
(* TODO: Write your own tests for the binomial function.
         See the provided test for how to write test cases.
         Remember that we assume that  n >= k >= 0; you should not write test cases where this assumption is violated.
*)
let binomial_tests = [
  (* Your test cases go here. Correct the incorrect test cases for the function. *)
  ((0, 0), 1);
  ((1, 0), 1);
  ((4, 2), 6);

]

(* TODO: Correct this implementation so that it compiles and returns
         the correct answers.
*)
(* Computes factorial*)
let rec factorial x = if x = 0 then 1 else x * factorial (x - 1)
                                             
let binomial n k =
  (factorial n) / ((factorial k) * (factorial (n - k)))




(* Question 3: Lucas Numbers *)

(* TODO: Write a good set of tests for lucas_tests. *)
let lucas_tests = [
  (0, 2);
  (1, 1);
]

(* TODO: Implement a tail-recursive helper lucas_helper. *) 
let rec lucas_helper first second n i = 
  if n = 0 then 2 else
  if n = 1 then 1 else
  if i = n then second else
    lucas_helper second (first + second) n (i + 1)

(* TODO: Implement lucas that calls the previous function. *)
let lucas n =
  lucas_helper 2 1 n 1
