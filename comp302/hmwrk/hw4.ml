(* Question 1: Tree Depth *)
(* TODO: Write a good set of tests for tree depth. *)
let tree_depth_cps_tests : (int tree * int) list =
  [ (Empty, 0);
    (Tree(Empty, 1, Empty), 1);
    (Tree(Tree(Empty, 1, Empty), 1, Tree(Tree(Empty, 1, Empty), 1, Empty)), 3);
  ]

(* An example of Non-CPS function to find depth of a tree: *)
let rec tree_depth t =
  match t with
  | Empty -> 0
  | Tree (l, _, r) -> 1 + max (tree_depth l) (tree_depth r)

(* TODO: Implement a CPS style tree_depth_cps function.*) 
let tree_depth_cps (t: 'a tree) = 
  let rec helper (t: 'a tree) (sc: (int -> int)) =
    match t with
    | Empty -> sc 0
    | Tree (l, _, r) ->  helper l (fun dl -> helper r (fun dr -> sc (1 + (max dl dr))))in
  helper t (fun x -> x)
(* Question 2(a): Tree Traversal *)
(* TODO: Write a good set of tests for testing your tree traversal function. *)
let traverse_tests : (int tree * int list) list =  [
  (Tree (Tree (Empty, 2, Empty), 1, Tree (Empty, 3, Empty)), [2; 3; 1]);
]

(* TODO: Implement a CPS style postorder traversal function. *)
let traverse (tree : 'a tree) : 'a list = 
  let rec helper (tree : 'a tree) (sc: (unit -> 'a list)) =
    match tree with
    | Empty -> sc ()
    | Tree (l, v, r) ->
        helper Empty (fun () -> (helper l sc) @ (helper r sc) @ [v]) in
  helper tree (fun () -> [])

(* Question 2(b): Distances from the Root *)
(* TODO: Write a good set of tests for testing the get_distances function. *)
let get_distances_tests : (int tree * int list) list = [
  (Tree (Tree (Empty, 3, Empty), 5, Tree (Empty, 6, Empty)), [8; 11; 5]);
]

(* TODO: Implement a CPS style get_distances function. *)
let get_distances (tree : int tree) : int list =
  let rec helper tree sum sc = 
    match tree with
    | Empty -> []
    | Tree (Empty, v, Empty) -> sc (sum + v)
    | Tree (l, v, r) ->
        helper (Tree (Empty, 0, Empty)) v (fun v -> (helper l (sum + v) sc) @ (helper r (sum + v) sc) @ [sum + v]) in
  helper tree 0 (fun x -> [x])

(* Question 3: Finding Subtrees *)
(* TODO: Write a good set of tests for finding subtrees. *)
let find_subtree_cps_tests : ((int list * int tree) * int tree option) list =
  [ (* Your test cases go here *)
    (([1;2], Empty), None);
    (([1;2], Tree(Empty, 1, Tree(Tree(Empty, 3, Empty), 2, Empty))), Some (Tree(Empty, 3, Empty)));
    (([1;2;3;5],Tree(Tree(Empty, 2, Tree(Empty, 3, Empty)), 1, Tree(Tree(Tree(Empty, 4, Empty), 3, Empty), 2, Empty))), None);
  ]
  

(* TODO: Implement a CPS style find_subtree_cont function.*)
let find_subtree_cps ls tree =
  let rec helper ls tree sc fc = 
    match ls with
    | [] -> sc tree
    | x :: xs -> match tree with
      | Empty -> fc ()
      | Tree (l, v, r) -> if (not (x = v)) then fc () else
            helper xs l sc (fun () -> helper xs r sc fc) in
  helper ls tree (fun x -> Some x) (fun () -> None)
  

