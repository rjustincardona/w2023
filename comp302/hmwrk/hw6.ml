(*--------------------------------------------------------------*)
(* Q1 : String to Characters to String                  *)
(*--------------------------------------------------------------*)

(* 1.1 Turn a string into a list of characters. *)
let string_explode (s : string) : char list = 
  List.map (String.get s) (tabulate (fun x -> x) (String.length s))
  

(* 1.2 Turn a list of characters into a string. *)
let string_implode (l : char list) : string =
  String.concat "" (List.map Char.escaped l)

(* ------------------------------------------------------------------------*)
(* Q2 : Bank Account *)
(* ------------------------------------------------------------------------*)
type bank_account' = {
  update_pass : password -> password -> unit;
  retrieve    : password -> int -> unit;
  deposit     : password -> int -> unit;
  show_balance : password -> int;
  balance: int;
  pass: password;
}

let open_account (pass: password) : bank_account =
  let is_open = ref true in
  let attempts = ref 0 in
  let balance = ref 0 in
  let passwd = ref pass in
  let check_pass x = 
    (
      if !is_open then
        (
          if x = !passwd then attempts:=0 else 
            (
              attempts := !attempts + 1; 
              if !attempts > 3 then (is_open := false; raise account_locked)
              else raise wrong_pass
            )
        )
      else raise account_locked
    ) in
  {
    update_pass=
      (
        fun x y ->
          if check_pass x = () then passwd := y else raise wrong_pass
      );
    retrieve=
      (
        fun x y -> 
          if !balance - y < 0 then raise not_enough_balance else
            (
              if y < 0 then raise negative_amount else
                (if check_pass x = () then balance := !balance - y else raise wrong_pass)
            )
      );
    deposit=
      (
        fun x y -> 
          if y < 0 then raise negative_amount else 
            (if check_pass x = () then balance := !balance + y else raise wrong_pass)
      );
    show_balance=
      (
        fun x -> 
          if check_pass x = () then !balance else raise wrong_pass
      )
  }

    


