(* Question 1 *)

(* TODO: Write a good set of tests for {!q1a_nat_of_int}. *)
let q1a_nat_of_int_tests : (int * nat) list = [
  (0, Z);
  (1, (S Z));
  (2, (S (S Z)));
]

(* TODO:  Implement {!q1a_nat_of_int} using a tail-recursive helper. *)
let rec q1a_nat_of_int' partial_ans n = 
  if n = 0 then partial_ans else
    q1a_nat_of_int' (S partial_ans) (n-1)
      
let q1a_nat_of_int (n : int) : nat = q1a_nat_of_int' Z n

(* TODO: Write a good set of tests for {!q1b_int_of_nat}. *)
let q1b_int_of_nat_tests : (nat * int) list = [
  (Z, 0);
  ((S Z), 1);
  ((S (S Z)), 2);
]

(* TODO:  Implement {!q1b_int_of_nat} using a tail-recursive helper. *)
let rec q1b_int_of_nat' (partial_ans : int) (n : nat) = 
  if n = q1a_nat_of_int partial_ans then partial_ans else
    q1b_int_of_nat' (partial_ans + 1) n
  
let q1b_int_of_nat (n : nat) : int = q1b_int_of_nat' 0 n

(* TODO: Write a good set of tests for {!q1c_add}. *)
let q1c_add_tests : ((nat * nat) * nat) list = [
  ((Z, Z), Z);
  ((Z, (S Z)), (S Z));
  (((S Z), (S Z)), (S (S Z)));
]

(* TODO: Implement {!q1c_add}. *)
let rec q1c_add (n : nat) (m : nat) : nat = 
  match m with
  | Z -> n
  | (S m) -> q1c_add (S n) m

(* Question 2 *)

(* TODO: Implement {!q2a_neg}. *)
let q2a_neg (e : exp) : exp = Times (e, Const (-1.))

(* TODO: Implement {!q2b_minus}. *)
let q2b_minus (e1 : exp) (e2 : exp) : exp = Plus(e1, q2a_neg e2)

(* TODO: Implement {!q2c_pow}. *)
let rec q2c_pow (e1 : exp) (p : nat) : exp = match p with
  | Z -> Const 1.0
  | S p -> Times(e1, q2c_pow e1 p)


(* Question 3 *)

(* TODO: Write a good set of tests for {!eval}. *)
let eval_tests : ((float * exp) * float) list = [
  ((3.0, Plus(Times(Const 2.0, Var), Var)), 9.0);
  ((5.0, Div(Times(Var, Var), Var)), 5.0);
]

(* TODO: Implement {!eval}. *)
let rec eval (a : float) (e : exp) : float = match e with
  | Const e -> e
  | Var -> a
  | Plus(e1, e2) -> (eval a e1) +. (eval a e2)
  | Times(e1, e2) -> (eval a e1) *. (eval a e2)
  | Div(e1, e2) -> (eval a e1) /. (eval a e2)


(* Question 4 *)

(* TODO: Write a good set of tests for {!diff_tests}. *)
let diff_tests : (exp * exp) list = [
  (Const 1., Const 0.);
  (Times(Var, Plus(Var, Const 1.)), Plus(Times(Const 1., Plus(Var, Const 1.)), Times(Var, Plus(Const 1., Const 0.))));
  (Times(Var, Var), Plus(Times(Const 1., Var), Times(Var, Const 1.)));
  (Div(Var, Var), Div(q2b_minus (Times(Const 1., Var)) (Times(Var, Const 1.)), Times(Var, Var)));
]

(* TODO: Implement {!diff}. *)
let rec diff (e : exp) : exp = match e with
  | Const e -> Const 0.
  | Var -> Const 1.
  | Plus(e1, e2) -> Plus(diff e1, diff e2)
  | Times(e1, e2) -> Plus(Times(diff e1, e2), Times(e1, diff e2))
  | Div(e1, e2) -> Div((q2b_minus (Times(diff e1, e2)) (Times(e1, diff e2))), (Times(e2, e2)))
                     
                     
