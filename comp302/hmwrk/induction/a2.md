
### Proof

By induction on `l1`.

#### Base case: l1 = []

```
LHS = sum_tr ([] @ l2) acc
    = sum_tr l2 acc             -- by definition of @
    = sum_tr [] (sum_tr l2 acc) -- by definition of sum_tr
    = RHS
```

#### Step case: l1 = x :: xs

```
IH: sum_tr (xs @ l2) acc = sum_tr xs (sum_tr l2 acc)

LHS = sum_tr ((x :: xs) @ l2) acc
    = sum_tr (x :: (xs @ l2)) acc       -- by definition of @
    = sum_tr (xs @ l2) (x + acc)        -- by definition of sum_tr
    = x + sum_tr (xs @ l2) acc          -- by Theorem 1
    = x + sum_tr xs (sum_tr l2 acc)     -- by IH
    = sum_tr xs (x + (sum_tr l2 acc))   -- by Theorem 1
    = sum_tr (x :: xs) (sum_tr l2 acc)  -- by definition of sum_tr
    = RHS
```