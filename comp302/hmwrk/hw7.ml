(* SECTION 1 *)

(* Question 1.1 *)
let rec take (n : int) (s : 'a stream) : 'a list = 
  if n = 0 then [] else
    s.head :: (take (n-1) (force s.tail))

(* Question 1.2 *)
let rec lucas1 = {
  head = 2;
  tail = Susp (fun () -> lucas2);
}
and lucas2 = {
  head = 1;
  tail = Susp (fun () -> add_streams lucas1 lucas2);
}

(* Question 1.3 *)
let rec unfold (f : 'a -> 'b * 'a) (seed : 'a) : 'b stream =
  let (x, y) = f seed in
  {
    head = x;
    tail = Susp (fun () -> unfold f y)
  }

(* Question 1.4 *)
let lucas : int stream =
  let y = ref 2 in
  {
    head = 2;
    tail = Susp (fun () -> unfold (fun x -> let z = (x, !y + x) in y:= x; z) 1);
  }

(* SECTION 2 *)

(* Question 2.1 *)
let rec scale (s1 : int stream) (n : int) : int stream =
  str_map (fun x -> x * n) s1 

let rec merge (s1 : 'a stream) (s2 : 'a stream) : 'a stream =
  let h1 = s1.head in
  let h2 = s2.head in
  if h1 < h2 then
    {
      head = h1;
      tail = Susp (fun () -> merge (force s1.tail) s2)
    }
  else if h1 > h2 then
    {
      head = h2;
      tail = Susp (fun () -> merge s1 (force s2.tail))
    }
  else
    merge s1 (force s2.tail)
    

(* Question 2.2 *)
let rec s = 
  {
    head = 1;
    tail = Susp (fun () -> merge (scale s 2) (merge (scale s 3) (scale s 5)));
  }
    

