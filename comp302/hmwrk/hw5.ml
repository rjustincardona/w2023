(** Question 1 *)

(* TODO: Add test cases. *)
let collect_variables_tests : (formula * Variable_set.t) list = [
  (Conjunction ((Negation (Variable ("a")), Disjunction((Variable ("c"), Negation (Variable ("b")))))), Variable_set.singleton("a") |> Variable_set.add("b") |> Variable_set.add("c")); 
  (Negation(Negation(Conjunction(Variable("a"), Variable("b")))), Variable_set.singleton("a") |> Variable_set.add("b"))
]

(* TODO: Implement the function. *)
let collect_variables (formula : formula) : Variable_set.t =
  let rec helper (formula : formula) (acc: Variable_set.t) : Variable_set.t = 
    match formula with
    | Variable (a) -> acc |> Variable_set.add (a) 
    | Conjunction (a, b) -> helper a (acc |> Variable_set.union (helper b acc)) 
    | Disjunction (a, b) -> helper a (acc |> Variable_set.union (helper b acc))
    | Negation (a) -> helper a acc in
  helper formula Variable_set.empty

(** Question 2 *)

(* TODO: Add test cases. *)
let eval_success_tests : ((truth_assignment * formula) * bool) list = [
  ((Variable_map.singleton("a") true, parse_formula "a & ~a"), false);
  ((Variable_map.singleton("a") true |> Variable_map.add("b") true, parse_formula "(~(a | ~(a | b)) | b) & (a | b)"), true); 
  ((Variable_map.singleton("a") false |> Variable_map.add("b") true |> Variable_map.add("c") true |> Variable_map.add("d") false,
    parse_formula "(a | ~b) & c"), false)
] 
(* TODO: Add test cases. *)
let eval_failure_tests : ((truth_assignment * formula) * exn) list = [
  ((Variable_map.singleton("b") true, parse_formula "a & ~a"), Unassigned_variable ("a"));]

(* TODO: Implement the function. *)
let rec eval (state : truth_assignment) (formula : formula) : bool = match formula with
  | Variable (a) -> (
      match state |> Variable_map.find_opt(a) with 
      | None -> raise (Unassigned_variable a)
      | Some x -> x
    )
  | Conjunction (a, b) ->
      let x = eval state a in
      let y = eval state b in
      x && y
  | Disjunction (a, b) -> 
      let x = eval state a in
      let y = eval state b in
      x || y
  | Negation (a) -> 
      let x = eval state a in
      not x
                    
(** Question 3 *)

(* TODO: Add test cases. *)
let find_satisfying_assignment_tests : (formula * truth_assignment option) list = [
  (parse_formula "a & ~a", None);
  (parse_formula "~a & b", Some (Variable_map.singleton("a") false |> Variable_map.add("b") true));
]

(* TODO: Implement the function. *)
let find_satisfying_assignment (formula : formula) : truth_assignment =
  let rec helper (formula : formula) (acc : truth_assignment) : truth_assignment = 
    try
      (
        match (eval acc formula) with
        | true -> acc
        | false -> raise Unsatisfiable_formula
      )
    with | Unassigned_variable a ->
      try (helper formula (acc |> Variable_map.add(a) false)) with
      | Unsatisfiable_formula -> 
          helper formula (acc |> Variable_map.add(a) true) in 
  helper formula Variable_map.empty




