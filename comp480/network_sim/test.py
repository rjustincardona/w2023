import sympy as sp
from sympy import diff

x1, x2 = sp.symbols('x1, x2')
f = sp.Function('f')
phi = f(x1, x2)
eq = diff(phi, x1) + phi
print(sp.pdsolve(eq))
