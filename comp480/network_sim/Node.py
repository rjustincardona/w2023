class Node:
    def __init__(self, val, id, neighbours=[], coordinates = None):
        self.val = val
        self.id = id
        self.neighbours = neighbours
        self.coordinates = coordinates
        self.valp = 0
        self.valpp = 0

    def get_id(self):
        return self.id
    def set_id(self, id):
        self.id = id

    def get_val(self):
        return self.val
    def set_val(self, val):
        self.val = val

    def get_neighbours(self):
        return self.neighbours
    def add_neighbour(self, n):
        self.neighbours.append(n)
    def remove_neighbour(self, n):
        self.neighbours.remove(n)

    def get_coordinates(self):
        return self.coordinates
    def set_coordinates(self, coordinates):
        self.coordinates = coordinates

    def get_valp(self):
        return self.valp
    def set_valp(self, valp):
        self.valp = valp

    def get_valpp(self):
        return self.valpp
    def set_valpp(self, valpp):
        self.valpp = valpp