from Node import *
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.animation import FuncAnimation
import numpy as np

class Graph:
    def __init__(self, m=0):
        self.nodes = []
        self.id_cur = 0
        self.cost = 0
        def update(node, t=1e-1, max_val=1, min_val=0, eps=0, p=1e-3, method='klein-gordon'):
            v = node.get_val()
            vp = node.get_valp()
            c = node.get_coordinates()
            neighbours = node.get_neighbours()
            points = []
            for i in range(len(neighbours)):
                n_temp = self.get_node(neighbours[i])
                r = n_temp.get_coordinates() - np.array(c)
                r = np.hypot(*r)
                z = n_temp.get_val() - v
                points.append([r, z])
            if len(points) % 2 != 0:
                points.append([0, 0])
            dx2 = 0
            dx1 = 0
            for i in range(0, len(points), 2):
                try:
                    dx2 += 2 * np.polyfit([0, points[i][0], points[i+1][0]], [0, points[i][1], points[i+1][1]], 2)[0]
                    dx1 += np.polyfit([0, points[i][0], points[i+1][0]], [0, points[i][1], points[i+1][1]], 2)[1]
                except:
                    continue

            # if method=='klein-gordon':
            #     new_val = (2 - (m * t)**2) * v - vp + (dx2 * (t**2))
            #     node.set_valp(v)
            # elif method=='wave':
            #     new_val = 2 * v - vp + (dx2 * (t**2))
            #     node.set_valp(v)
            # elif method=='damped':
            #     new_val = ((2 - t) * v) - ((1 + t) * vp) + (dx2 * (t**2))
            #     node.set_valp(v)
            # if new_val > max_val:
            #     node.set_val(max_val - eps)
            # elif new_val < min_val:
            #     node.set_val(min_val + eps)
            # else:
            #     node.set_val(new_val)                 
            points = np.array(points)
            if np.random.rand() < p:
                n = np.random.choice(self.node_list())
                if n in node.get_neighbours():
                    node.remove_neighbour(n.get_id())
                    n.remove_neighbour(node.get_id())
                else:
                    node.add_neighbour(n.get_id())
                    n.add_neighbour(node.get_id())
            if np.random.rand() < p:
                v += np.random.rand() - 0.5
            return dx1**2 - ((node.get_val() - v) / t)**2 + (m*node.get_val())**2
        self.update = update

    def node_list(self):
        return self.nodes
    def get_node(self, id):
        return [n for n in self.node_list() if n.id == id][0]

    def add_node(self, val, neighbours, coordinates = None):
        temp_node = Node(val, self.id_cur, neighbours, coordinates=coordinates)
        self.nodes.append(temp_node)
        for n in neighbours:
            self.get_node(n).add_neighbour(self.id_cur)
        self.id_cur += 1   

    def remove_node(self, id):
        node_temp = self.get_node(id)
        self.nodes.remove(node_temp)
        for n in node_temp.neighbours:
            self.get_node(n).remove_neighbour(id)

    def get_edges(self):
        edges = []
        for i in self.node_list():
            for j in i.get_neighbours():
                edge_temp = [i.get_id(), j]
                if (edge_temp in edges) == False and (edge_temp.reverse() in edges) == False:
                    edges.append(edge_temp)
        return edges

    def plot_graph(self, option='2d', method='klein-gordon'):
        if option=='3d':
            coords = np.array([i.get_coordinates() for i in self.node_list()])
            vals = np.array([i.get_val() for i in self.node_list()])
            coords = np.append(coords.T, [vals], axis=0)
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            surf = ax.plot_trisurf(coords[0], coords[1],  coords[2], cmap=cm.jet, linewidth=0)
            fig.colorbar(surf)
            fig.tight_layout()
            plt.show()
        elif option=='2d':
            edges = self.get_edges()
            coords = [i.get_coordinates() for i in self.node_list()]
            edge_coords = [[self.get_node(i).get_coordinates() for i in j] for j in edges]
            for i in coords:
                plt.scatter(i[0], i[1], s=60., linewidths=0.5, c="#61D836", edgecolors="black", zorder=1)
            for i in edge_coords:
                i  = np.array(i)
                plt.plot(i.T[0], i.T[1], color="black", linewidth=1, alpha=0.2, zorder=0)
            plt.show()
        elif option=='animation':
            coords = np.array([i.get_coordinates() for i in self.node_list()])
            vals = np.array([i.get_val() for i in self.node_list()])
            coords = np.append(coords.T, [vals], axis=0)
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            def animate( n):
                ax.cla()
                for n in self.node_list():
                    self.cost += self.update(n, method=method)
                coords = np.array([i.get_coordinates() for i in self.node_list()])
                vals = np.array([i.get_val() for i in self.node_list()])
                coords = np.append(coords.T, [vals], axis=0)
                ax.plot_trisurf(coords[0], coords[1], coords[2], cmap=cm.jet, linewidth=0)
                return fig,
            anim = FuncAnimation(fig = fig, func = animate, frames = 300, interval = 1, repeat = False)
            anim.save("animation.gif",fps=30)
            return self.cost
