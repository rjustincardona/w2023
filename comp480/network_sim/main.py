from Graph import *
import numpy as np

M = 1e-1
def make_2d_grid(N):
    graph = Graph(m=M)
    def mesh2d(x, y):
        return np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])
        
    mesh = mesh2d(np.arange(N), np.arange(N))
    for m in mesh:
            graph.add_node(np.random.rand(), [], m)

    for n in graph.node_list():
        id = n.get_id()
        col = id % N
        row = id - col
        l = row + ((col - 1) % N)
        r = row + ((col + 1) % N)
        t = ((row - N) % (N**2)) + col
        b = ((row + N) % (N**2)) + col
        for i in [l, r, b, t]:
            n.add_neighbour(i)         
    return graph

cost_static = []
cost_wave = []
count = 0
while True:
    graph = make_2d_grid(10)
    m = M
    cost = 0
    for node in graph.node_list():
        v = node.get_val()
        c = node.get_coordinates()
        neighbours = node.get_neighbours()
        points = []
        for i in range(len(neighbours)):
             n_temp = graph.get_node(neighbours[i])
             r = n_temp.get_coordinates() - np.array(c)
             r = np.hypot(*r)
             z = n_temp.get_val() - v
             points.append([r, z])
        if len(points) % 2 != 0:
            points.append([0, 0])
        dx1 = 0
        for i in range(0, len(points), 2):
            try:
                dx1 += np.polyfit([0, points[i][0], points[i+1][0]], [0, points[i][1], points[i+1][1]], 2)[1]
            except:
                continue
        cost += dx1**2 + (m * v)**2
    cost_wave.append(graph.plot_graph(option='animation', method='klein-gordon'))
    cost_static.append(cost)
    count += 1
    if count == 100:
        break

print("Static: " + str(np.mean(cost_static)) + " \pm " + str(np.std(cost_static) / np.sqrt(len(cost_static))))
print("Wave: " + str(np.mean(cost_wave)) + " \pm " + str(np.std(cost_wave) / np.sqrt(len(cost_wave))))
